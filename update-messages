#!/usr/bin/env bash

# This script extracts translations (into locale/messages.pot),
# updates existing translations (locale/$lang/messages.po)
# and fails if a conflict happened.
#
# @author Florian Dold <dold@taler.net>

SCRIPTPATH="$( cd -- "$(dirname "$0")" >/dev/null 2>&1 ; pwd -P )"
set -eu

root=$PWD
sitegen=$SCRIPTPATH

cd $root

languages=$($sitegen/list-languages)

echo Using languages $languages

echo "Updating message catalog"
env "PYTHONPATH=$sitegen:${PYTHONPATH-:}" pybabel extract -F locale/babel.map -o locale/messages.pot~ .

diffcount=$(diff locale/messages.pot locale/messages.pot~ | grep "^>" | wc -l)

# Only copy pot file if more than the timestamp changed
if [[ $diffcount -gt 1 ]]; then
  cp locale/messages.pot~ locale/messages.pot
fi


echo "Updating existing translations"
for lang in $languages; do
        msgmerge -q -U -m --previous locale/$lang/LC_MESSAGES/messages.po locale/messages.pot
done

# Check for conflicts
if grep -nA1 '#-#-#-#-#' locale/*/LC_MESSAGES/messages.po; then
        echo "ERROR: Conflicts encountered in PO files.";
        exit 1;
fi

for lang in $languages; do
  pybabel -q compile -d locale -l $lang --use-fuzzy
done
